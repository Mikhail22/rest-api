# Пробую в деле Jib

### Использовал статью
https://www.baeldung.com/jib-dockerizing

### Документация
https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin

### Деплой
Для деплоя в registry в терминале выполнить
#### mvn compile jib:build

Для авто-деплоя в Idea надо снаять комментарий в pom.xml executions -> execution
