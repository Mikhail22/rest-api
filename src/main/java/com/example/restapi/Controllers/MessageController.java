package com.example.restapi.Controllers;

import com.example.restapi.model.Message;
import com.example.restapi.study.Lesson01;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RestController
@RequestMapping(
        path = "/",
        method = RequestMethod.GET
//        consumes = {}
)

public class MessageController {

    @Autowired
    Lesson01 lesson01;

    @GetMapping("messages")
    public List<Message> getMessages(){

        log.info("/messages");
        lesson01.start();
        List<Message> messages = Stream.of(new Message(1,"message 1")
                ,new Message(2,"message 2")
                ,new Message(3,"message 3")
                ,new Message(4,"message 4")
                ,new Message(5,"message 5")
                ,new Message(6,"message 6")
                ,new Message(7,"message 7")
                ).collect(Collectors.toList());
        return messages;

    }

}
