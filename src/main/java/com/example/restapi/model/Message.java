package com.example.restapi.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
public class Message {
    private final Integer id;
    private final String message;

}
