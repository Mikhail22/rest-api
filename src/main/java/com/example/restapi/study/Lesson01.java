package com.example.restapi.study;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class Lesson01 {

    @Autowired
//    @HandleEnabled
    List<MyHandler> handlers;
    Number n;

    public void start(){
        log.info("Start");
        handlers.forEach(h -> h.doIt());
    }


}
