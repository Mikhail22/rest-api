package com.example.restapi.study;

import org.springframework.stereotype.Component;

@Component
public class MyHandlerImpl2 implements MyHandler {
    @Override
    public void doIt() {
        System.out.println("doId 2");
    }
}
