package com.example.restapi.study;

import com.example.restapi.config.ErrorMessages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@HandleEnabled
@Slf4j
@Profile("prod")
public class MyHandlerImpl1 implements MyHandler {
    @Autowired
    ErrorMessages errorMessages;

    @Override
    public void doIt() {
        System.out.println("doId 1 with qualifier");
        errorMessages.getUsers().forEach((user)-> System.out.println(user));
        log.info("INFO MESSAGE doId 1 with qualifier");
        log.debug("DEBUG MESSAGE doId 1 with qualifier");
        log.error(errorMessages.getErrorMessage());
        log.warn("WARN MESSAGE doId 1 with qualifier");
    }
}
