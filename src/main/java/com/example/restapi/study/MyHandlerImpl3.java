package com.example.restapi.study;

import org.springframework.stereotype.Component;

@Component
@HandleEnabled
public class MyHandlerImpl3 implements MyHandler {
    @Override
    public void doIt() {
        System.out.println("doId 3 with qualifier");
    }
}
