package com.example.restapi.study;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("prod")
class Lesson01Test {
    @Autowired
    Lesson01 lesson01;


    @Test
    public void start(){
        lesson01.start();
    }

}