канал ADV-IT
https://www.youtube.com/watch?v=kGwe8IEDiX4&list=PLg5SS_4L6LYvN1RqaVesof8KAf-02fJSi&index=8

https://kubernetes.io/docs/tasks/tools/install-kubectl/

minikube start
kubectl get nodes
kubectl get pods

-- эту обычно не используют
kubectl run hello --generator=run-pod/v1 --image=adv4000/k8sphp:latest --port=80

-- подробное описание пода
kubectl describe pods hello

-- войти в под
kubectl exec -it hello sh

-- логи
kubectl logs hello

--удалить под
kubectl delete pods hello

-- создать под из файла
kubectl apply -f pod-myapp.yaml

-- удалить под из файла
kubectl delete -f pod-myapp.yaml

-- перенаправление порта пода
kubectl port-forward rest-api 8888:8080


minikube stop
